package pl.edu.pwsztar;


import java.util.LinkedList;
import java.util.List;

class Bank implements BankOperation {

    private int accountNumber = 0;
    private List<Account> accounts = new LinkedList<>();

    public int createAccount() {
        accountNumber++;
        accounts.add(new Account(accountNumber,0));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        int pos = AccountFinder.find(accounts,accountNumber);
        if(pos == -2){
            return ACCOUNT_NOT_EXISTS;
        }
        else {
            int balance = accounts.get(pos).getBalance();
            accounts.remove(pos);
            return balance;
        }
    }

    public boolean deposit(int accountNumber, int amount) {
        int pos = AccountFinder.find(accounts,accountNumber);
        if(pos == -2){
            return false;
        } else {
            accounts.get(pos).setBalance(accounts.get(pos).getBalance()+amount);
            return true;
        }
    }

    public boolean withdraw(int accountNumber, int amount) {
        int pos = AccountFinder.find(accounts,accountNumber);
        if(pos == -2){
            return false;
        }
        if(accounts.get(pos).getBalance()<amount){
            return false;
        }
        accounts.get(pos).setBalance(accounts.get(pos).getBalance()-amount);
        return true;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        int posfrom = AccountFinder.find(accounts,fromAccount);
        int posto = AccountFinder.find(accounts,toAccount);

        if(posto == -2 || posfrom == -2){
            return false;
        }
        if(accounts.get(posfrom).getBalance()<amount){
            return false;
        }

        accounts.get(posfrom).setBalance(accounts.get(posfrom).getBalance()-amount);
        accounts.get(posto).setBalance(accounts.get(posto).getBalance()+amount);
        return true;
    }

    public int accountBalance(int accountNumber) {
        int pos = AccountFinder.find(accounts,accountNumber);
        if (pos == -2){
            return ACCOUNT_NOT_EXISTS;
        }
        return accounts.get(pos).getBalance();
    }

    public int sumAccountsBalance() {
        int total  = 0;
        for(Account account: accounts){
            total = total + account.getBalance();
        }
        return total;
    }
}
