package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositMoneySpec extends Specification{
    @Unroll
    def 'Shoud be able to deposit money on account #acc'(){
        given:
        def bank = new Bank()
        bank.createAccount()
        bank.createAccount()
        when:
        def exist = bank.deposit(acc,cash)
        then:
        exist == expected

        where:
        acc | cash  || expected
        1   | 100   || true
        2   | 200   || true
        3   | 300   || false
    }
}
