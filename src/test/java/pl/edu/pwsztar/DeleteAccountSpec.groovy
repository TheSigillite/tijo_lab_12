package pl.edu.pwsztar

import org.junit.Before
import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification{

    @Unroll
    def 'Shoud delete account number #acc'(){
        given: 'init'
            def bank = new Bank();
            bank.createAccount();
            bank.createAccount();
            bank.createAccount();
        when: 'Account is deleted'
            def number = bank.deleteAccount(acc)
        then: 'Shoud return account balance'
            number == expected

        where:
        acc || expected
        1   || 0
        2   || 0
        4   || -1
    }
}
