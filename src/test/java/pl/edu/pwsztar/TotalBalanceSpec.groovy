package pl.edu.pwsztar

import spock.lang.Specification

class TotalBalanceSpec extends Specification{

    def 'Shoud be able to retrieve balance'(){
        given:
            def bank = new Bank()
            bank.createAccount()
            bank.deposit(1,100)
            bank.createAccount()
            bank.deposit(2,300)
        when:
            def total = bank.sumAccountsBalance()
        then:
            total == 400
    }

    def 'Shoud return 0 when no accounts'(){
        given:
            def bank = new Bank()
        when:
            def total = bank.sumAccountsBalance()
        then:
            total == 0
    }
}
