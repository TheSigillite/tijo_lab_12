package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferMoneySpec extends Specification{
    @Unroll
    def 'Shoud be able to transfer #amount from acc #accfrom to #accto'(){
        given:
        def bank = new Bank()
        bank.createAccount()
        bank.createAccount()
        bank.deposit(1,300)
        bank.deposit(2,100)
        when:
        def result = bank.transfer(accfrom,accto,amount)
        then:
        result == expected

        where:
        accfrom | accto | amount || expected
        1   |   2   |   100 |   true
        2   |   1   |   200 |   false
        3   |   1   |   100 |   false
    }
}
