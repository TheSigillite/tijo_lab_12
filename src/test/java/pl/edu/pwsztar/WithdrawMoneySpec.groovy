package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawMoneySpec extends Specification{
    @Unroll
    def 'Shoud be able to withdraw #cash from account #acc'(){
        given:
        def bank = new Bank();
        bank.createAccount();
        bank.deposit(1,300)
        bank.createAccount();
        bank.deposit(2,50)
        when:
        def withdraw = bank.withdraw(acc,cash)
        then:
        withdraw == expected

        where:
        acc | cash  || expected
        1   | 200   || true
        2   | 100   || false
        3   | 100   || false
    }
}
