package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class CheckBalanceSpec extends Specification{
    @Unroll
    def 'Shoud be able to check balance on account #acc'(){
        given:
            def bank = new Bank();
            bank.createAccount();
            bank.createAccount();
            bank.deposit(1,200);
            bank.deposit(2, 100);
        when:
            def balance = bank.accountBalance(acc);
        then:
            balance == expected

        where:
        acc ||   expected
        1   ||  200
        2   ||  100
        3   || -1
    }
}
